import datetime
import re
from django.shortcuts import render
from .forms import UserLoginForm, UserRegisterForm, addNumberForm
from django.contrib.auth import login, logout
from django.shortcuts import redirect, render
from django.contrib import messages
from .models import Phone_user,Tarif
from django.contrib.auth.models import User


UserModel = User.objects.all()
PhoneUserModel = Phone_user.objects.all()
TariffsModel = Tarif.objects.all()

# Create your views here.


def index(request):
    return render(request, "operatorApp/main.html", )


def LoginPanel(request):
    if request.method == "POST":
        form = UserLoginForm(data=request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)

            return redirect('main')
        else:

            form = UserLoginForm()
            messages.error(
                request, f"Помилка авторизації")
    else:
        form = UserLoginForm()
    return render(request, "operatorApp/login.html", {'form': form})


def registrPanel(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Реєстрація відбулась успішно")
            return redirect('login')
        else:
            form = UserRegisterForm()
            messages.error(request, "Помилка реєстрації")
    else:
        form = UserRegisterForm()

    return render(request, "operatorApp/register.html", {'form': form})


def addPhoneNumber(request):
    phoneNumber=request.POST.get("phone_number")
    if not request.user.is_authenticated:
        messages.error(request,"Користувач не авторизований")
        return redirect("main")
    isPhoneAdded = PhoneUserModel.filter(user_id=request.user.id)
    if isPhoneAdded.exists():
        return render(request, "operatorApp/connectPhoneNumber.html", {"phone_number": isPhoneAdded[0].phone_number})
    
    if request.method == "GET":
        form = addNumberForm()
        return render(request, "operatorApp/connectPhoneNumber.html", {'form': form})
    
    if request.method == "POST":
        if not re.match("^\+?3?8?(0\d{9})$",phoneNumber ):
            messages.error(request, "Номер не пройшов валідацію, перевірте правильність введення")
            return redirect("addNumber")


        newPuser = PhoneUserModel.create(phone_number=phoneNumber, user_id=request.user.id)
        return render(request, "operatorApp/connectPhoneNumber.html", {"phone_number": newPuser.phone_number})


def deletePhoneNumber(request):
    if not request.user.is_authenticated:
        messages.error(request,"Користувач не авторизований")
        return redirect("main")
    print(request.method)
    if request.method == "GET":
        user = PhoneUserModel.filter(user_id=request.user.id)
        user.delete()
        return redirect("addNumber")

    return redirect("main")



def userInfo(request):
    if not request.user.is_authenticated:
        messages.error(request,"Користувач не авторизований")
        return redirect("main")
    print(request.method)
    if request.method == "GET":
        user = PhoneUserModel.filter(user_id=request.user.id)
        
        if user.exists():
            return render(request, "operatorApp/profile.html", {"phone_user": user[0]})
        return render(request, "operatorApp/profile.html")
    return redirect("main")

def tarifList(request):

    if request.method == "GET":
        return render(request, "operatorApp/tarifs.html", {"tariffs":TariffsModel })
    return redirect("main")
def tarifPage(request,id):
    tarif = TariffsModel.get(pk=id)
    if request.method == "GET":
        return render(request, "operatorApp/tarifPage.html", {"tariff":tarif })
    return redirect("main")


def updatetarif(request,id):
    if not request.user.is_authenticated:
        messages.error(request,"Користувач не авторизований")
        return redirect("main")
    print(request.method)
    if request.method == "GET":
        user = PhoneUserModel.filter(user_id=request.user.id)
        if user.exists():
            user=user[0]
        else:
            messages.error(request, "Даний користувач не існує")
            return redirect("main")            
        tarif= TariffsModel.filter(pk=id)
        if not tarif:
            messages.error(request, "Даний тариф не існує")
            return redirect("main")
        if user.balance< tarif[0].price:
            messages.error(request, f"У вас не вистачає коштів на даний тариф\nЦіна тарифу:{tarif[0].price}\nБаланс користувача:{user.balance}")
            return redirect("main")
        user.tarif=tarif[0]
        user.balance-=tarif[0].price
        user.last_payed_tarif=datetime.datetime.now()
        user.save()
        messages.success(request, "Тариф оновлено")
        return redirect(f"/tarif/{id}")
    return redirect("main")


def addbalance(request):
    if not request.user.is_authenticated:
        return redirect("main")
    print(request.method)
    user = PhoneUserModel.filter(user_id=request.user.id)[0]

    if request.method == "GET":
        return render(request,"operatorApp/addBalance.html",{"user":user})
    if request.method == "POST":
        user.balance+=float(request.POST.get("inputAmount"))
        user.save()
        return redirect("addbalance")
        pass
    