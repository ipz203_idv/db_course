from dataclasses import field
from logging import PlaceHolder
from statistics import mode
from tkinter import Widget
from turtle import textinput
from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm,UserChangeForm,PasswordChangeForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from .models import *

class UserRegisterForm(UserCreationForm):

    username = forms.CharField(label="Ім'я користувача(Nickname)" , widget=forms.TextInput(attrs={'class':'form-control'}))
    password1 =  forms.CharField(label="Пароль" , widget=forms.PasswordInput(attrs={'class':'form-control'}))
    password2 =  forms.CharField(label="Повторіть пароль" , widget=forms.PasswordInput(attrs={'class':'form-control'}))
    class Meta:
        model = User
        fields = ('username','password1','password2')

class UserLoginForm(AuthenticationForm):
    username = forms.CharField(label="Ім'я користувача(Nickname)", widget=forms.TextInput(
        attrs={'class': 'form-control'}))
    password = forms.CharField(
        label="Пароль", widget=forms.PasswordInput(attrs={'class': 'form-control'}))


class addNumberForm(forms.Form):
    phone_number = forms.CharField(label="Номер телефона", widget=forms.TextInput(
        attrs={'class': 'form-control'}))

