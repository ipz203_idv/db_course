from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator

# Create your models here.


class Passport(models.Model):
    fullname = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    number = models.IntegerField()

    def __str__(self):
        return f"{self.fullname} {self.number}"


class Studnet(models.Model):
    fullname = models.CharField(max_length=100)
    number = models.IntegerField()

    def __str__(self):
        return f"{self.fullname} {self.number}"


class Tarif(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=400)
    price = models.IntegerField()
    photo_path = models.ImageField(upload_to='tarif_images/')
    internet_kb_count = models.FloatField()
    sms_count = models.IntegerField()
    minutes_count = models.IntegerField()

    def __str__(self):
        return f"{self.name} {self.price}"


class Phone_user(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    phone_number = models.IntegerField(max_length=13, null=False,
                                       validators=[RegexValidator(
                                           regex=r'^\+?3?8?(0\d{9})$',
                                           message="Phone number must be entered in the format: '+380912476524'"
                                       )])
    photo_path = models.ImageField(upload_to="user_images/")
    last_payed_tarif = models.DateField(blank=True, null=True)
    balance = models.FloatField(default=0)
    passport = models.ForeignKey(Passport, on_delete=models.CASCADE, blank=True, null=True)
    tarif = models.ForeignKey(Tarif, on_delete=models.CASCADE, blank=True, null=True)
    student = models.ForeignKey(Studnet, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return f"{self.phone_number}"


class Used(models.Model):
    DATA_TYPES = [
        ('kb', 'kb'),
        ('min', 'min'),
        ('sms', 'sms'),
    ]

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    type = models.CharField(max_length=5, choices=DATA_TYPES)
    used = models.FloatField()
