# Generated by Django 4.2.2 on 2024-03-17 22:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('operatorApp', '0002_alter_phone_user_last_payed_tarif'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phone_user',
            name='passport',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='operatorApp.passport'),
        ),
        migrations.AlterField(
            model_name='phone_user',
            name='student',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='operatorApp.studnet'),
        ),
        migrations.AlterField(
            model_name='phone_user',
            name='tarif',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='operatorApp.tarif'),
        ),
    ]
