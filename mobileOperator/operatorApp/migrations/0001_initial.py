# Generated by Django 4.2.2 on 2024-03-17 22:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Passport',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fullname', models.CharField(max_length=100)),
                ('address', models.CharField(max_length=100)),
                ('number', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Studnet',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fullname', models.CharField(max_length=100)),
                ('number', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Tarif',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=400)),
                ('price', models.IntegerField()),
                ('photo_path', models.ImageField(upload_to='tarif_images/')),
                ('internet_kb_count', models.FloatField()),
                ('sms_count', models.IntegerField()),
                ('minutes_count', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Used',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('kb', 'kb'), ('min', 'min'), ('sms', 'sms')], max_length=5)),
                ('used', models.FloatField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Phone_user',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone_number', models.CharField(max_length=13)),
                ('photo_path', models.ImageField(upload_to='user_images/')),
                ('last_payed_tarif', models.DateField()),
                ('balance', models.FloatField(default=0)),
                ('passport', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='operatorApp.passport')),
                ('student', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='operatorApp.studnet')),
                ('tarif', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='operatorApp.tarif')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
