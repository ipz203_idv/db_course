
from django.urls import path
from . import views 
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('',views.index,name="main"),
    path('user/login',views.LoginPanel,name="login"),
    path('user/register',views.registrPanel,name="register"),
    path('user/number/add',views.addPhoneNumber,name="addNumber"),
    path('user/number/delete',views.deletePhoneNumber,name="deleteNumber"),
    path('user/info',views.userInfo,name="cabinet"),
    path('tarifs',views.tarifList,name="tarifs"),
    path('tarif/<int:id>',views.tarifPage),
    path('user/tarif/<int:id>/update',views.updatetarif),
    path('user/balance/add',views.addbalance,name="addbalance")

] 
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT) 